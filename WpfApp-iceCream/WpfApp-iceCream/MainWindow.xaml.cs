﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp_iceCream
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            const string dataFile = @"..\..\flavors.txt";
            if (File.Exists(dataFile))
            {
                List<string> flavorsList = new List<string>();
                foreach (string line in File.ReadAllLines(dataFile))
                {
                    flavorsList.Add(line);
                }
                lvIceCreamFlavors.ItemsSource = flavorsList;
            }
        }


        List<string> selectedOnes = new List<string>();

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            lvSelectedFlavors.ItemsSource = selectedOnes;  // binding the source, instead of re-assign for each time

            //.SelectedIndex: Gets or sets the index of the first item in the current selection or returns. (-1) if the selection is empty.
            if (lvIceCreamFlavors.SelectedIndex == -1)
            {
                MessageBox.Show("You need to choose a flavor");
                return;
            }
            var selectedForAdd = lvIceCreamFlavors.SelectedItems;

            foreach (string item in selectedForAdd)
            {
                selectedOnes.Add(item);
            }

            //lvSelectedFlavors.ItemsSource = null;   // remove the previous assigned source and set to null            
            //lvSelectedFlavors.ItemsSource = selectedOnes;  

            lvSelectedFlavors.Items.Refresh();  // refresh the GUI

            lvIceCreamFlavors.SelectedIndex = -1;  // remove the "select graphic"

        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvSelectedFlavors.Items.Count == 0)
            {
                return;
            }
            if (lvSelectedFlavors.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose a flavor");
                return;
            }

            var selectedForDelete = lvSelectedFlavors.SelectedItems;
            foreach (string item in selectedForDelete)
            {
                selectedOnes.Remove(item);
            }

            //  lvSelectedFlavors.ItemsSource = null;      
            lvSelectedFlavors.ItemsSource = selectedOnes;
            lvSelectedFlavors.Items.Refresh();

            lvSelectedFlavors.SelectedIndex = -1;

        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            // validate if items are already selected or not

            if (lvSelectedFlavors.Items.Count == 0)
            {
                return;
            }

            // confirm before execute clear
            MessageBoxResult result = MessageBox.Show("Are you sure to clear the list?", "Clear all", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                lvSelectedFlavors.ItemsSource = null;
                selectedOnes.Clear();   // remove all items from selectedOnes
            }
        }
    }
}
