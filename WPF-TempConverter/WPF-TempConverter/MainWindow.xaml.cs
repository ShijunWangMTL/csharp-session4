﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_TempConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        
        private void inTemp_TextChanged(object sender, TextChangedEventArgs e)
        {
            tempConv();
        }


        private void rbtn_Checked(object sender, RoutedEventArgs e)
        {
            tempConv();
        }

        private void tempConv()
        {
            // read input temperature
            string tempStr = txtInTemp.Text;
            double inTemp, outTemp;
            if (!double.TryParse(tempStr, out inTemp) && (tempStr != null))
            {
                MessageBox.Show("Input value must be a number.");
            }

            if (rbtnInC.IsChecked == true)
            {
                if (rbtnOutC.IsChecked == true)
                {
                    showOutTemp.Content = inTemp + "°C";
                }
                if (rbtnOutF.IsChecked == true)
                {
                    outTemp = inTemp * 9 / 5;
                    showOutTemp.Content = outTemp + "°F";
                }
                if (rbtnOutK.IsChecked == true)
                {
                    outTemp = inTemp + 273.15;  
                    showOutTemp.Content = outTemp + "K";
                }
            }

            if (rbtnInF.IsChecked == true)
            {
                if (rbtnOutF.IsChecked == true)
                {
                    showOutTemp.Content = inTemp + "°F";
                }
                if (rbtnOutC.IsChecked == true)
                {
                    outTemp = (inTemp - 32) * 5/9;
                    showOutTemp.Content = outTemp + "°C";
                }
                if (rbtnOutK.IsChecked == true)
                {
                    outTemp = (inTemp - 32) + 273.15;
                    showOutTemp.Content = outTemp + "K";
                }
            }

            if (rbtnInK.IsChecked == true)
            {
                if (rbtnOutK.IsChecked == true)
                {
                    showOutTemp.Content = inTemp + "K";
                }
                if (rbtnOutF.IsChecked == true)
                {
                    outTemp = (inTemp - 273.15) * 9 / 5 + 32;
                    showOutTemp.Content = outTemp + "°F";
                }
                if (rbtnOutC.IsChecked == true)
                {
                    outTemp = inTemp - 273.15;
                    showOutTemp.Content = outTemp + "°C";
                }
            }
        }



    }
}
